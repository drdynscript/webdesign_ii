//NAMELESS FUNCTION - Auto executed when document has loaded
(function () {
    console.log("Mega template ^_^ in HTML5 & CSS3");//LOG IN CONSOLE TAB DEVTOOLS

    //SCROLL TO TOP
    $('.scrolltotop').click(function (ev) {
        ev.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, 600);
        return false;
    });

    //FADE IN AND OUT SCROLLTOTOP
    $(window).scroll(function (ev) {
        if ($(this).scrollTop() > 60)
            $('.scrolltotop').fadeIn();
        else
            $('.scrolltotop').fadeOut();
    });
})();
