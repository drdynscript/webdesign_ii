Webdesign II
=====================

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot, Bart Missant, Arnaud Coolsaet, Lieven Van Speybroeck|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

## Lesinhouden

|Lesweek|Omschrijving|
|----|----------|
|1|Inleiding, Groepsindeling, Briefing Opdracht 1, Herhalingsoefeningen|
|2|Webontwerpen Deel1, Herhalingsoefeningen| 
|3|Webonterpen Deel2, Herhalingsoefeningen|
|4|Werkweek|
|5|Structuur Webapplicatie, CSS3 transformaties, transities en animaties, jsFiddle of Codepen.io als webeditor|
|6|Implementatie Grid-systeem, Briefing Opdracht 2|
|7|Navigatiestructuren|
|8|Werkweek|
|-|Paasvakantie, Deadline Opdracht 2|
|9|Briefing Opdracht 3, Werkweek|
|10|Werkweek|
|11|Werkweek|
|12|Werkweek|
|13|Werkweek, Deadline Opdracht 3|

## Cursus

* [Webdesign-II Inleiding](https://bitbucket.org/drdynscript/webdesign_ii/src/cc00a04ce4eefdcc23371785cb92e39e489ecf7e/wd_inleiding.pdf?at=master)
* [Webontwerpen - Deel 1](https://bitbucket.org/drdynscript/webdesign_ii/src/cc00a04ce4eefdcc23371785cb92e39e489ecf7e/wd_webontwerpen_deel1.pdf?at=master)
* [Webontwerpen - Deel 2](https://bitbucket.org/drdynscript/webdesign_ii/src/cc00a04ce4eefdcc23371785cb92e39e489ecf7e/wd_webontwerpen_deel2.pdf?at=master)
* [Structuur van een webapplicatie, incl. HTML5 Template](https://bitbucket.org/drdynscript/webdesign_ii/src/68f455fbb56839c2094772a8c0dfe5fce849ce87/wd_webappstructure.pptx?at=master)


##Opdrachten

* [Opdracht 1: reUX](https://bitbucket.org/drdynscript/webdesign_ii/src/cc00a04ce4eefdcc23371785cb92e39e489ecf7e/wd_opdracht1.pdf?at=master)


## Applicaties

* [Herhalingsoefeningen Webdesign II & III](https://bitbucket.org/drdynscript/webdesign_ii/src/68f455fbb56839c2094772a8c0dfe5fce849ce87/Oefeningen/?at=master)
* [Example folderstructure](https://bitbucket.org/drdynscript/webdesign_ii/src/68f455fbb56839c2094772a8c0dfe5fce849ce87/ex_folderstructure/?at=master)
* [HTML5 Template](https://bitbucket.org/drdynscript/webdesign_ii/src/cc00a04ce4eefdcc23371785cb92e39e489ecf7e/ex_html5template/?at=master)