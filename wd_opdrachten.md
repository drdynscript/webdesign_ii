Opdrachten
=========

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot, Bart Missant, Arnaud Colsaet, Lieven Van Speybroeck|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|
|Opleidingsonderdeel|Webdesign-II|

***
[TOC]
***

Briefing Opdracht 2: Pimp My Site
-----------------------------------------------------------------

|Info||
|----|--|
|Werktitel|Pimp My Site|
|Subtitel|GDM's One Page Application|
|Briefing|**17-03-2015** Briefing Opdracht 2|
|Opleveren|**10-04-2015** Opleveren opdracht 2|
|Groepswerk|Maximaal 3 studenten|

###Structuur dossier

**Onderdelen + vereisten productiedossier**:

* Voorblad (Opleiding, Arteveldehogeschool, Academiejaar, Opleidingsonderdeel, Opdracht 2: Pimp My Site, GDM's One Page Application, Groepsnummer en groepsleden
* Inhoudsopgave
* Elke pagina bevat:
	* Hoofding
		* Webdesign II - Opdracht 2: Pimp My Site
	* Footer
		* Bachelor in de grafische en digitale media
		* 2014-15 Arteveldehogeschool
		* Paginanummer 

**Structuur of inhoudsopgave opdracht 1**:

* Ideeënborden
* Wireframe
* Style Tiles
	* Aantal: 1 (groep bestanden uit 1 student), 2 (groep bestaande uit 2 studenten), 3 (groep bestaande uit 3 studenten)
	* Finale Style Tile (groep van 2 à 3 studenten)
* Screen Designs
	* Aantal: 1 (groep bestanden uit 1 student), 2 (groep bestaande uit 2 studenten), 3 (groep bestaande uit 3 studenten)
	* Finaal Screen Designs (groep van 2 à 3 studenten)
* Screenshots
	* Eindresultaat na implementatie
	* Code snippet van CSS
	* Code snippet van HTML


###Structuur applicatie

* **De structuur van deze opdracht ligt vast!**
* Implementatie
	* Class en Id attributen toevoegen aan de HTML.
	* Andere wijzigingen zijn niet toegelaten!
	* Enkel common.css, grid.css en home.css aanpassen!
* Design
	* Uniek design voor GDM
	* Arteveldehogeschool huisstijl mag niet gebruikt worden!
	* Andere designs mogelijkheden: favicons, touch icons, sprites, iconen, achtergrondafbeeldingen, webfonts, ...

###Opleveren

- Chamilo (zip bestand - Naam zip bestand: **webdesign_201415_pimpmysite_{ahs groepsnr}.zip**)
    - dossier.pdf
    - pimpmysite (folder)

###Quotering

* 40% Design, 60% Implementatie



