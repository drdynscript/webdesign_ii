﻿Design principles in Webdesign
================

[TOC]

##Lay-outs

###Fixed lay-out

Een **fixed lay-out** in webdesign is een lay-out waarbij de meeteenheid niet afhankelijk is van andere factoren, zoals de fontgrootte of de afmetingen van het browser-window, om de breedte van een webpagina te bepalen. De webpagina bevat een vaste breedte en verandert niet wanneer we:

- het browser-venster verkleinen of vergroten.
- fontgrootte aanpassen.

Meestal wordt de **absolute meeteenheid ** uitgedrukt in **pixels (px)** voor beeld en in **points (pt)**, **centimeters (cm), ...** voor print. Een **fixed grid lay-out** is een fixed lay-out die een **grid** gebruikt voor de verschillende kolommen binnen een webpagina. Een kolom bevat eveneens een vaste breedte uitgedrukt in pixels. In een fixed lay-out verandert de lay-out niet onder verschillende omstandigheden zoals bijv. bij verschillende resoluties.

Op dit moment[^now] gaan we ervan uit dat de meeste Internet gebruikers een resolutie[^pollw3] gebruiken op de desktop van minimaal **1366x768pixels** of hoger. Ongeveer 20% gebruikt een resolutie die lager is. Een grid-grootte van **1140pixels of 960pixels** worden algemeen aanvaard als vaste of maximum breedte van een webpagina. Een breedte van 960 pixels past goed op schermen met resolutie 1024x768pixels en er is nog plaats voor een kleine linker- en rechter marge, die adem-of witruimte geven.

**Voorbeeldcode fixed lay-out**

```html
<div class="wrapper">
     <div class="container">
         ...
     </div>	
 </div>
```

```css
 * {
     box-sizing:border-box;
 }
 .wrapper{
     width:100%;
     margin:0;
     padding:0;
 }
 .container{
     width:1140px;
     margin:0 auto;
}
```

In het bovenstaande voorbeeld hebben we een container gedefinieerd met een vaste breedte van 1140 pixels. Wijzigen van de fontgrootte en/of het browservenster hebben geen invloed op de breedte van deze container.



###Relative lay-out

Een relatieve lay-out is een lay-out waarbij de meeteenheid afhankelijk is van andere factoren, zoals de fontgrootte of de afmetingen van het browser-window, om de breedte van een webpagina te bepalen. De webpagina bevat een een variabele breedte en verandert wanneer we:

- het browser-venster verkleinen of vergroten.
- fontgrootte aanpassen.

De webpagina spreidt zich om de volledige breedte van het browser-venster te vullen gelijkaardig met een vloeistof (liquid) die zich verspreidt over de gehele oppervlakte van een container wanneer we deze vloeistof hierin gieten. Meestal wordt de **relatieve meeteenheid ** uitgedrukt in **percentages (%)**, **em** of **rem**  voor beeld en print. Indien de breedte van een webpagina wordt ingesteld op 86%, betekent dit 86% van de maximum breedte die het kan bevatten. Em is de hoogte van het basis-font gebruikt in een webpagina. 

> Veronderstellen we dat we een basis fontgrootte gebruiken van 16px, dan betekent 2.5em voor de fontgrootte van een h1: `16*2.5=40px`. 2.5em wordt relatief bekeken ten opzichte van 16px.

Voor echt heel grote of heel kleine schermen is deze lay-out niet echt om aan te zien.

> **Voorbeeld http://forum.politics.be:**
>
> Grote schermen
![enter image description here](https://lh6.googleusercontent.com/-h4eqPLhjpCU/U_2R-z_B51I/AAAAAAAAAG4/xl9BYmYAFcE/s0/forum_large.PNG "forum_large.PNG")
> Heel kleine schermen
![enter image description here](https://lh4.googleusercontent.com/-npmDOFg9K88/U_2SFuiYLOI/AAAAAAAAAHM/81TJpqWbLso/s0/forum_small.PNG "forum_small.PNG")
>

Andere benamingen voor een relative lay-out zijn: 

- Liquid lay-out
- Fluid lay-out
- Flexible lay-out
- Elastic lay-out

Een **fluid of liquid lay-out is een lay-out die de relatieve eenheid %** gebruikt. Een **elastic lay-out is een schaalbare lay-out op basis van standaard fontgrootte**. Relatieve eenheid gebaseerd op de font-size eigenschap uitgedrukt in **em of rem**. Indien we geen standaard font opgeven, dan zal het standaard font uit de webbrowser gebruikt worden. Meestal is dit het font "Times New Roman" met een grootte van 16 pixels.

> In Chrome via `chrome://settings/fonts`
>
> ![enter image description here](https://lh4.googleusercontent.com/-31UFrFjqR-8/U_2j4YJVEGI/AAAAAAAAAHo/iTwDORcbXrs/s0/font_chrome.PNG "font_chrome.PNG")
> 
> ![enter image description here](https://lh4.googleusercontent.com/-3HwQcnzR-Sg/U_2kAmEEF1I/AAAAAAAAAH0/ypVT480tYNw/s0/font_chrome_2.PNG "font_chrome_2.PNG")

De gebruiker kan dus ten alle tijden dit lettertype via de browserinstellingen aanpassen. Deze instelling heeft dus rechtstreekse invloed op de lay-out en typo van een webpagina.

### Grid-systeem

> *..a structure comprising a series of horizontal and vertical lines, used to arrange content.*

Een grid-systeem is een structuur bestaande uit horizontale- en verticale lijnen en wordt vervolgens gebruikt om inhoud erop uit te lijnen. Het wordt door designers gebruikt om inhoud en afbeeldingen gestructureerd te presenteren. 

Een grid-systeem geeft een solide basis om een lay-out voor designs te creëren voor webpagina's die uniform en consistent zijn doorheen de webpagina's waardoor de gebruikerservaring verbetert. 

Grid-systeem in webdesign bevat een aantal herkenbare termen, namelijk: **wrapper, container, row, column, gutter en offset**.

Een **wrapper** is een html-element die de volledige breedte omsluit van het browservenster omsluit, bevat geen padding of marges.

Een **container** is een html-element dat een absolute of relatieve breedte heeft en wordt gecentreerd binnen de wrapper. Een wrapper bevat slechts één container. Een container kan een linker- en rechter padding bevatten.

Een **row** is een html-element dat een absolute of relatieve breedte heeft en kan een negatieve linker- en rechter marge bevatten. Een container bevat één of meerdere row-elementen.

Een **column** is een html-element waarin inhoud wordt geplaatst. Deze inhoud kan over meerdere kolommen verspreid worden.

Een **gutter** is de afstand tussen de kolommen. De linker- en rechter padding van een rij is meestal gelijk aan de breedte van een gutter.

Een **offset** is de afstand die een kolom moet verplaatsen in de horizontale richting ten opzichte van de linker- of rechter kant van een rij.

> 
> $
> CONw = nc*COLw + nc*Gw + 2*CONpw + 2*ROWmw
> $
> 
> Waarbij:
> 
> - $nc$: aantal kolommen in de grid
> - $COLw$: de breedte van één kolom
> - $Gw$: de breedte van een gutter
> - $CONw$: de breedte van de container
> - $CONpw$: de breedte van de padding in horizontale richting van de container
> - $ROWmw$: de breedte van de marge in horizontale richting van een rij

Vermits meestal het aantal kolommen, de breedte van de container en de breedte van één gutter gekend zijn, kunnen we hierdoor de breedte van één kolom berkenen.

> $
> COLw = (CONw - nc*Gw - 2*CONpw - 2*ROWmw)/nc
> $



###Fixed lay-out met een grid-systeem

Voor de aanmaak van een fixed lay-out m.b.v. een grid-systeem bepalen we eerste de breedte van de container en van de gutter. In dit voorbeeld heeft de container een breedte van 1140 pixels en de gutter 24 pixels. De linker- en rechter padding van een container bedraagt de helft van de gutter, namelijk 12 pixels. Een rij bevat geen marges.

Via de universele selector (*) in CSS geven we aan alle html-elementen een initiële marge van 0 pixels en aan de padding ook 0 pixels. Daarnaast maken de linker- en rechter padding alsook de border breedte links en rechts deel uit van de opgegeven of gecalculeerde breedte van een html-element en dit via de eigenschap `box-sizing` met als waarde `border-box`. De container heeft een breedte van 1140 pixels en wordt centraal geplaatst in het browservenster.

**CSS voor de herkenbare termen uit een grid-systeem:**

```css
* {
    box-sizing: border-box;
    margin:0;
    padding:0;
}
.wrapper, .container, .row, .col {
    display:block;
}
.wrapper{
    width:100%;
}
.container{
    width:1140px;
    margin:auto;
    padding:0 12px;
}
.row{
    margin:0;
}
.row:before,
.row:after {
    content: " ";
    display: table;
}
.row:after {
    clear: both;
}
.row .row {
    margin:0 -12px;
}
.col{
    float:left;
    margin:0 12px 24px;
    overflow:hidden;
}
```

Alle kolommen worden links zwevend geplaatst en hebben een linker- en rechtermarge gelijk aan de helft van een gutter. De marge onder een kolom is gelijk aan de gutter-breedte. Alle inhoud binnen een kolom, die buiten de n de kolombreedte liggen worden afgesneden via `overflow:hidden`.

Het grid-systeem dat we willen maken moeten ook geneste grids ondersteunen. Dit betekent dat een kolom een rij kan bevatten en binnen deze laatste kunnen we terug kolommen definiëren. Dit principe is ook beter gekend als **nested grids**. Om dit te realiseren in CSS moeten we negatieve linker- en rechter marges toekennen aan de geneste rijen die gelijk zijn aan de breedte van een gutter. In ons voorbeeld bedragen deze marges -24 pixels.

In ons grid-systeem gebruiken wij 12 kolommen. Alle gegevens zijn nu gekend om de breedte van één kolom te berekenen via:

> $
> COLw = (CONw - nc*Gw - 2*CONpw - 2*ROWmw)/nc
> $
> 
> $
> COLw = (1140 - 12*24 - 2*12 - 2*0)/12 = 69
> $

De kolombreedte bedraagt dus 69 pixels. Deze waarde is noodzakelijk om alle kolommen te bereken uit deze grid, gaande van 12 kolommen tot 1 kolom.

> $
> Cew = noc*COLw + (noc-1)*Gw
> $
> 
> Voorbeeld noc = 6
> 
> $
> Cew = 6*69 + (6-1)*24 = 534
> $

Deze werkwijze passen we ook toe om de andere kolommen te berekenen, dit resulteert in:

|$noc$|$Cew$|
|-----|-----|
|12|1092|
|11|999|
|10|906|
|9|813|
|8|720|
|7|627|
|6|534|
|5|441|
|4|348|
|3|255|
|2|162|
|1|69|

De waarden kennen we vervolgens toe aan de corresponderende selectoren in CSS.

```css
.col-12{
    width:1092px;
}
.col-11{
    width:999px;
}
.col-10{
    width:906px;
}
.col-9{
    width:813px;
}
.col-8{
    width:720px;
}
.col-7{
    width:627px;
}
.col-6{
    width:534px;
}
.col-5{
    width:441px;
}
.col-4{
    width:348px;
}
.col-3{
    width:255px;
}
.col-2{
    width:162px;
}
.col-1{
    width:69px;
}
```

Om het grid-systeem visueel te testen kennen we nog een aantal kleuren toe aan de kolommen en de geneste kolommen.

```css
html{
    background:#333;
}
.col{
    background:#c0c0c0;
}
.col .col{
    background:#a0a0a0;
}
.col .col .col{
    background:#808080;
}
.col .col .col .col{
    background:#606060;
}
```

Vervolgens maken we de webpagina aan waarmee we de grid kunnen testen.

```html
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col col-12">col-12</div>
            <div class="col col-11">col-11</div>
            <div class="col col-1">col-1</div>
            <div class="col col-10">col-11</div>
            <div class="col col-2">col-2</div>
            <div class="col col-9">col-9</div>
            <div class="col col-3">col-3</div>
            <div class="col col-8">col-8</div>
            <div class="col col-4">col-4</div>
            <div class="col col-7">col-7</div>
            <div class="col col-5">col-5</div>
            <div class="col col-6">col-6</div>
            <div class="col col-6">col-6</div>
            <div class="col col-4">col-4</div>
            <div class="col col-4">col-4</div>
            <div class="col col-4">col-4</div>
            <div class="col col-3">col-3</div>
            <div class="col col-3">col-3</div>
            <div class="col col-3">col-3</div>
            <div class="col col-3">col-3</div>
            <div class="col col-2">col-2</div>
            <div class="col col-2">col-2</div>
            <div class="col col-2">col-2</div>
            <div class="col col-2">col-2</div>
            <div class="col col-2">col-2</div>
            <div class="col col-2">col-2</div>
            <div class="col col-1">col-1</div>
            <div class="col col-1">col-1</div>
            <div class="col col-1">col-1</div>
            <div class="col col-1">col-1</div>
            <div class="col col-1">col-1</div>
            <div class="col col-1">col-1</div>
            <div class="col col-1">col-1</div>
            <div class="col col-1">col-1</div>
            <div class="col col-1">col-1</div>
            <div class="col col-1">col-1</div>
            <div class="col col-1">col-1</div>
            <div class="col col-1">col-1</div>
        </div>
    </div>
</div>

<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col col-12">
                col-12
                <div class="row">
                    <div class="col col-6">col-6</div>
                    <div class="col col-6">
                    col-6
                        <div class="row">
                            <div class="col col-3">col-3</div>
                            <div class="col col-3">
                                col-3
                                <div class="row">
                                    <div class="col col-2">col-2</div>
                                    <div class="col col-1">col-1</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
```


> **Output fixed grid lay-out**
> 
> ![enter image description here](https://lh4.googleusercontent.com/-Umzp9OwAXMI/U_8fq62pYGI/AAAAAAAAAIU/6w81C4g4gpU/s0/fixed_gridlayout.PNG "fixed_gridlayout.PNG")
> 

We merken op dat de som van de geneste kolommen gelijk is aan het aantal kolommen van de ouder (parent), bijv.: ouder col-3, de geneste kolommen col-2 en col-1.


##Responsive webdesign

Voorheen investeerde vele bedrijven in één aparte mobiele website, die inhoudelijk in grote mate gelijk was aan de originele desktop-website.  Door het vele aanbod aan resoluties volstaat één mobile website niet meer. In plaats van te werken met absolute vaste afmetingen is het veel nuttiger om websites te implementeren met relatieve eenheden, zoals: percentages, em of rem.

Responsive webdesign is een aanpak in webdesign om te streven naar een optimale weergave van de website voor een breed aanbod aan devices, zoals: desktops, smartphones, tablets, ... . Het design van een webpagina past zich aan aan de resolutie van het gebruikte toestel of device. De webpagina schaalt hierdoor mee met de afmetingen van het scherm zonder in te boeten op het gebied van gebruiksvriendelijkheid, leesbaarheid en de gebruikerservaring.

Een responsive website gebruikt o.a.:

- een fluid of elastic lay-out;
- flexibele afbeeldingen, video's en andere geïncorporeerde assets;
- CSS3 media queries.

Een fluid grid gerbuikt relatieve eenheden, zoals percentages, om het grid-systeem op te bouwen. Flexibele afbeeldingen zijn afbeeldingen waarvan de schaal zodanig wordt aangepast, zodat deze passen binnen hun container. Media queries laat ons toe om andere CSS-stijlregels te gebruiken gebaseerd op de karakteristieken van het toestel. De meest gehanteerde karakteristiek is de breedte van de browser op het toestel.

###Progressive enhancement

Progressive enhancement is een strategie in webdesign en omvat toegankelijkheid, semantische html-markup, externe stijl- en scriptbestanden. Het gaat ervan uit dat iedereen toegang heeft tot de basisinhoud en basisfunctionaliteit van een webpagina via een browser. Afhankelijk van de mogelijkheden van smartphones, tablets of pc's wordt deze basis webpagina uitgebreid met extra features. Deze werkt dus overal goed dit in tegenstelling tot **graceful degradation** waarbij we starten met een webpagina die alle inhoud en functionaliteiten bevat waarin we alle functionaliteiten uitschakelen die niet door het toestel wordt ondersteund na het optreden van fouten.

**Voorbeeld _Print This_ uit de website w3.org**
```
<p id="printthis">
<a href="javascript:window.print()">Print this page</a>
</p>
```


Links om een een webpagina uit te printen zijn een beetje nutteloos, omdat de gebruiker meestal het print-icoon hanteert. Ze kunnen wel nuttig zijn op het einde van een bepaalde wizard, bijv. om een reis te bestellen. Om deze links te realiseren is JavaScript noodzakelijk en dit via de `print()` methode uit het `window` object. Dit werkt enkel indien JavaScript toegepast kan worden. Wanneer de gebruiker JavaScript heeft uitgeschakeld, zal deze link en dus ook de actie die daaraan verbonden is niet werken. Dit resulteert in frustraties bij de gebruiker.

```
<p id="printthis">
  <a href="javascript:window.print()">Print this page</a>
</p>
<noscript>
  <p class="scriptwarning">
    Print a copy of your confirmation. 
    Select the "Print" icon in your browser,
    or select "Print" from the "File" menu.
  </p>
</noscript>
```

In het bovenstaande voorbeeld kunnen we beter de **gracefull degradation** aanpaak toepassen: vertel de gebruiker dat de link misschien niet zal werken om een welbepaalde reden of suggereer een alternatieve oplossing om de webpagina toch uit te printen.

Een betere aanpak is om een printknop pas te voorzien indien dit ondersteund wordt door de browser van de cliënt. **Progressive enhancement** betekent dat we pas functionaliteit aanbieden indien dit kan toegepast worden bij de cliënt.

```
<p id="printthis">Thank you for your order. Please print this page for your records.</p>
<script type="text/javascript">
(function(){
  if(document.getElementById){
    var pt = document.getElementById('printthis');
    if(pt && typeof window.print === 'function'){
      var but = document.createElement('input');
      but.setAttribute('type','button');
      but.setAttribute('value','Print this now');
      but.onclick = function(){
        window.print();
      };
      pt.appendChild(but);
    }
  }
})();
</script>
```

Indien de container, waarin we de toekomstige op toevoegen, aanwezig is en de globale functie `print()` aanspreekbaar is, genereren we een input-element van het button-type met als waarde: "Print this now". Vervolgens koppelen we een luisteraar aan dit input-element die luistert naar de gebeurtenis "click". In deze luisteraar roepen we de functie `window.print()` op, waardoor het document zal uitgeprint worden. We vergeten natuurlijk niet om dit input-element toe te voegen aan de container met `id="print-this"`.

In **mobile-first webdesign** wordt eerst de basisinhoud en functionaliteit ontwikkeld die werken op de 1ste generatie mobiele toestellen. Daarna wordt deze inhoud en functionaliteit uitgebreid afhankelijk van de features die het mobiel toestel of pc ondersteunen. Het is daarom dat ook de termen progressive enhancement en mobile-first webdesign nauw verweven zijn met responsive webdesign.

###Media queries

Een media query bestaat uit een mediatype en tenminste één expressie die de scope van stijlbestanden beperkt door gebruik van media-eigenschappen, zoals: width, height en kleur. 

Media queries laten toe om de presentatie van inhoud aan te passen aan en specifiek bereik van toestellen zonder dat de inhoud wordt gewijzigd. De html-structuur blijft ongewijzigd.

Een expressie resulteert in true of false. Het resultaat van een media querie is true indien het het gespecificeerde mediatype gelijk is aan het device-type, waarop de webpagina weergegeven wordt, en alle expressies true zijn.

De mediatypen die ondersteund worden in CSS zijn:

- `all`
	Geschikt  voor alle toestellen.
- `print`
	Geschikt voor paged media en documenten die bekeken worden via de print preview mode. Klaar om uitgeprint te worden.
- `screen`
	Geschikt voor computermonitoren.
- `speech`
	Bedoeld voor speech synthesizers.
- `aural`, `braille`, `projection`, `tv` en `tty`

```css
@media screen, print {
  body { line-height: 2.4em }
}
```

Indien we geen mediatype specificeren, dan wordt de waarde `all` aangenomen.  

```
<!-- CSS media query on a link element -->
<link rel="stylesheet" media="(max-width: 767px)" href="main_767.css" />

<!-- CSS media query within a stylesheet -->
<style>
@media (max-width: 767px) {
  #sidebar-right {
    background:#d0e0f0;
  }
}
</style>
```

CSS media queries kunnen toegevoegd worden aan een stylesheet link-element of binnen een stylesheet. Vergeet niet dat alle gelinkte stijlbestanden toch gedownload worden desondanks de media querie `false` is. Het gelinkte stijlbestand zal echter niet toegepast worden.

Complex media queries zijn mogelijk door gebruik te maken van logische operatoren: `not`, `and` en `only`.  De `and` operator wordt gebruikt om verschillende media features te combineren tot een enkele media querie. Alle media features moeten `true` zijn om de gecombineerde media querie `true` te maken. `not` en `only` operatoren moeten gebruikt worden bij opgegeven mediatype.

Verschillende media queries kunnen ook gecombineerd worden in een komma-gescheiden lijst. Hierin met minstens één van de media features `true` zijn om de gecombineerde media query `true` te maken.

```css
@media screen and (min-width: 768px) and (orientation: landscape) { ... }
```

De bovenstaande meda query zal `true` zijn indien het mediatype `screen` is, de minimum breedte van de viewport 768px bedraagt en de displaymode in landscape is.

```css
@media (min-width: 768px), handheld and (orientation: portrait) { ... }
```
Wanneer we op een device werken met een scherm en de viewpoort minimaal 768px bedraagt, dan zal de media querie `true` zijn. Maken we gebruik van een handheld device in portrait modus, dan zal de media query ook waar of `true` zijn.

```css
@media not screen and (color), print and (color) { ... }
```

Het `not` keyword is van toepassing op heel de media query, behalve als deze gescheiden wordt door een komma. Het bovenstaande wordt geëvalueerd als:

```css
@media (not (screen and (color))), print and (color) { ... }
```

Als het geen computermonitor is en geen kleuren bevat, dan is de media query `true`. Als het mediatype `print` is en kleuren bevatten dan is de media query ook `true`.

Naast het instellen van de mediatypen kunnen we bijkomen media features opgeven. De meeste media features kunnen voorzien worden van de prefixen `min-` (groter of gelijk aan) of `max-` (kleiner of gelijk aan). Indien een media feature niet van toepassingen is voor een device, dan zal de expressie altijd `false` zijn. 

In grid-systemen beperken we ons tot twee media features, namelijk `min-width` en `max-width` met het mediatype `screen`.  Met deze media feature `width` specificeren we de breedte van de viewport, de breedte van het document window of de breedte van de pagina-box op een printer. Voor het mediatype screen zijn er een aantal courante media queries voorhanden, die ook in responsive frameworks, zoals Twitter Bootstrap en Foundation Zurb, worden toegepast.

Er zijn twee technieken om webapplicaties te realiseren afhankelijk van het toestel, namelijk mobile-first of desktop-first. In mobile-first ontwerpen en implementeren we de webapplicatie zoals ze te bekijken zijn op een smartphone. In desktop-first ontwerpen en implementeren we de webapplicatie zoals ze te bekijken zijn op een desktop pc.

```css

	/* MOBILE FIRST */
	
	/* Extra Extra Small Devices (maximum width 479px), Phones, Default for Mobile-First no Media Query needed */ 

	/* Extra Small Devices, Phones */ 
	@media only screen and (min-width : 480px) {}

	/* Small Devices, Tablets */
	@media only screen and (min-width : 768px) {}

	/* Medium Devices, Desktops */
	@media only screen and (min-width : 992px) {}

	/* Large Devices, Wide Screens */
	@media only screen and (min-width : 1200px) {}
```

In mobile-first worden devices met een viewport kleiner dan 480px gezien als extra extra small devices. Extra small devices zijn toestellen waarvan de breedte varieert tussen 480px en 767px. Voor tablets hanteren we een viewport-breedte tussen 768px en 991px. Voor desktop-schermen hanteren we een breedte tussen 992px en 1199px. Groteren en wijde schermen beginnen vanaf een breedte van 1200px.

```css

	/* DESKTOP FIRST */
	
	/* Large Devices, Wide Screens (maximum width 1200px), Default for Desktop-First no Media Query needed */ 
	
	/* Medium Devices, Desktops */
	@media only screen and (max-width : 992px) {} 
	
	/* Small Devices, Tablets */
	@media only screen and (max-width : 768px) {}

	/* Extra Small Devices, Phones */ 
	@media only screen and (max-width : 480px) {}

	/* Extra Extra Small Devices, Phones */ 
	@media only screen and (max-width : 320px) {}	
```
Bij desktop-first vertrekken we vanuit de hoogste resolutie en voegen we vervolgens media queries toe tot en met het kleinste device, namelijk kleiner of gelijk aan 320px.

Er zijn heel wat media features die in CSS ondersteund worden:

- `color, min-color en max-color`
Aantal bits per kleurcomponent.
- `color-index, min-color-index en max-color-index`
Aantal mogelijke kleuren uit de color look-up table
-` aspect-ratio, min-aspect-ratio en max-aspect-ratio`
Ratio tussen de breedte en de hoogte van de viewport van het device, bv.: 9/16.
- `device-aspect-ratio, min-device-aspect-ratio en max-device-aspect-ratio`
Ratio tussen de breedte en de hoogte van het device, bv.: 9/16.
- `device-height, min-device-height en max-device-height`
De hoogte van het device, de volledige schermhoogte.
- `device-width, min-device-width en max-device-width`
De breedte van het device, de volledige schermbreedte.
- `orientation`
Mogelijke waarden voor de viewport `portrait` of `landscape`. 
- `resolution, min-resolution en max-resolution`
De resolution van het device gespecificeerd als dots-per-inch (dpi), dots-per-centimeter (dpcm) of dots-per-pixel (dppx).

##Pseudoinhoud voor interfaces

- **Lorem Ipsum**
Gebaseerd op paragrafen, woorden, bytes en lijsten
<http://www.lipsum.com/>
- **Gansta Lorem Ipsum**
Artisanale filter tekst. Gebaseerd op paragrafen.
<http://lorizzle.nl/?feed=1>
- **Hipster Ipsum**
Artisanale filter tekst. Gebaseerd op paragrafen.
<http://hipsteripsum.me/>
- **Bacon Ipsum**
Tekst met vlees als topic. Ideaal voor de vegetariërs onder ons :). Gebaseerd op paragrafen.
<http://baconipsum.com/>
- **html ipsum**
Paragrafen, ongeordende lijsten (unordered lists), empty tables, form, definition list, navigation, …. Ideaal als pseudo content voor de ontwikkeling van een webapplicatie.
<http://html-ipsum.com/>
- **SAMUEL L. IPSUM**
Paragrafen, Headings en “Fucking Bitchass” paragrafen tags. Nostalgische momenten naar de film “Pulp Fiction”.
<http://slipsum.com/>
- **CUPCAKE IPSUM**
Paragrafen al dan niet with “Love”. Voor de zoetekiemen onder ons.
<http://cupcakeipsum.com/>
- **En we kunnen gebruik maken van nog veel meer ipsums :)**
<http://chooseyouripsum.com/>, <http://www.pixel-push.com/2012/08/06/best-dummy-text-and-lorem-ipsum-generators/>
- **HTML5 ipsums?**
Een begin via <http://loripsum.net/>. Misschien een project voor GDM? Een GDM ipsum?
- **Lorempixel**
Lorem Ipsum voor afbeeldingen. We wachten nooit op beeldmateriaal, we gebruiken het reeds beschikbare tijdens de testfase, in dit geval een online service. Deze service laat toe om de dimensie van een afbeelding te bepalen, alsook de categorie, dummy tekst en de identiteit binnen deze categorie.
<http://lorempixel.com/>
- **Wat met het integreren van video’s?**
Geen Ipsum gedoe meer, we kunnen gebruik maken van YouTube en/of Vimeo. Pseudo is veel gezegd, maar vele video’s zijn alvast fake, ideaal om te testen in webapplicaties. Vimeo en YouTube hebben hun eigen principe om hun speler te integreren in een web-pplicatie, doch zijn beiden gebaseerd op iframes! Eén van de volgende sessies bevat het integreren van beide videoservices.
<http://www.youtube.com>, <http://www.vimeo.com>

##Bibliografie

- http://www.thesitewizard.com/webdesign/liquid-elastic-fixed-relative-layout.shtml
- http://www.smashingmagazine.com/2009/06/02/fixed-vs-fluid-vs-elastic-layout-whats-the-right-one-for-you/
- http://www.smashingmagazine.com/2008/06/26/flexible-layouts-challenge-for-the-future/?cp=6
- http://webdesign.tutsplus.com/articles/all-about-grid-systems--webdesign-14471
- http://liquidapsive.com/
- http://www.responsys.com/blogs/nsm/cross-channel-marketing/difference-responsive-adaptive-web-design/
- http://venturebeat.com/2013/11/19/responsive-design-adaptive/
- http://marketingland.com/is-adaptive-web-design-or-ress-better-than-responsive-web-design-for-seo-59389
- http://visual.ly/adaptive-web-design-vs-responsive-web-design
- http://www.webmonkey.com/2013/05/the-two-flavors-of-a-one-web-approach-responsive-vs-adaptive/
- http://www.sitepoint.com/moving-beyond-responsive-web-adaptive-web/
- http://www.smashingmagazine.com/2014/05/14/responsive-images-done-right-guide-picture-srcset/
- http://www.smashingmagazine.com/2014/05/12/picturefill-2-0-responsive-images-and-the-perfect-polyfill/

>
> **Graceful degredation en progressive enhancement:**
> 
- http://searchnetworking.techtarget.com/definition/graceful-degradation
- http://en.wikipedia.org/wiki/Progressive_enhancement
- http://en.wikipedia.org/wiki/Responsive_web_design
- http://en.wikipedia.org/wiki/Fault_tolerance
- http://www.w3.org/wiki/Graceful_degredation_versus_progressive_enhancement
>
> **Media queries:**
> 
- https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Media_queries
- http://dev.w3.org/csswg/mediaqueries/#mf-dimensions
- http://code-tricks.com/css-media-queries-for-common-devices/

[^now]: 26/08/2014

[^pollw3]: Poll resoluties w3schools: http://www.w3schools.com/browsers/browsers_display.asp

> Written with [StackEdit](https://stackedit.io/).